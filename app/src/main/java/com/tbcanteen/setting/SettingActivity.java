package com.tbcanteen.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.tbcanteen.R;
import com.tbcanteen.lunch.LunchPlanRepo;

import java.util.concurrent.Executors;

public class SettingActivity extends AppCompatActivity implements SettingFragment.OnSettingFragmentListener {

    static Context appContext;
    static final String FRAGMENT_SETTING_TAG = "FRAGMENT_SETTING_TAG";
    public static final String PREF_SETTING_DB = "PREF_SETTING_DB";
    public static final String PREF_SETTING_IMAGE_AS_BACKGROUND = "PREF_SETTING_IMAGE_AS_BACKGROUND";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appContext = getApplicationContext();

        setContentView(R.layout.activity_setting);

        Toolbar myToolbar = findViewById(R.id.activity_setting_toolbar);
        myToolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorTradebyteBlue)));
        myToolbar.setTitle(R.string.menu_setting_title);
        setSupportActionBar(myToolbar);

        // Provide to go "up" in navBar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null) {
            return;
        }

        if (findViewById(R.id.activity_setting_fragment_container) != null) {
            SettingFragment fragment = new SettingFragment();

            getSupportFragmentManager()
                .beginTransaction()
                .add(
                    R.id.activity_setting_fragment_container,
                    fragment,
                    FRAGMENT_SETTING_TAG
                )
                .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goBack();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        NavUtils.navigateUpFromSameTask(this);
    }

    public void deleteDatabase() {
        LunchPlanRepo lunchPlanRepo = new LunchPlanRepo(appContext, Executors.newSingleThreadExecutor());
        lunchPlanRepo.deleteAll();

        Toast.makeText(this, getString(R.string.database_has_been_truncated), Toast.LENGTH_LONG).show();

        new Handler().postDelayed(
            () -> Toast.makeText(
                appContext,
                getString(R.string.there_will_be_no_plans_saved),
                Toast.LENGTH_LONG
            ).show(),
            3000
        );
    }

    /**
     * getDefaultSharedPreferences(Context) -> used for whole app.
     * getPreferences(Context)              -> used for only for "this" activity.
     * getSharedPreferences(Name, Context)  -> used for custom "Name" preferences-files.
     *
     * @param key Key for saving data.
     */
    public void saveSettingBool(String key, Boolean value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(appContext);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        //editor.commit(); // writes synchronously
        editor.apply(); // writes asynchronously
    }

    public static Boolean getSettingBool(Context appContext, String key, Boolean defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(appContext);

        return sharedPref.getBoolean(key, defaultValue);
    }
}
