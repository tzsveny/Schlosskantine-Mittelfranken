package com.tbcanteen.setting;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.tbcanteen.R;
import com.tbcanteen.lunch.LunchPlan;
import com.tbcanteen.lunch.LunchPlanRepo;

import java.util.concurrent.Executors;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    private OnSettingFragmentListener mListener;
    private CompositeDisposable disposables = new CompositeDisposable();

    public interface OnSettingFragmentListener {
        void deleteDatabase();
        void saveSettingBool(String key, Boolean value);
    }

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        Switch switchUseDb = view.findViewById(R.id.switch_use_database);
        switchUseDb.setChecked(SettingActivity.getSettingBool(getContext(), SettingActivity.PREF_SETTING_DB, true));
        switchUseDb.setOnCheckedChangeListener(switchUseDbListener);

        Switch switchImageAsBackground = view.findViewById(R.id.switch_image_as_background);
        switchImageAsBackground.setChecked(SettingActivity.getSettingBool(getContext(), SettingActivity.PREF_SETTING_IMAGE_AS_BACKGROUND, true));
        switchImageAsBackground.setOnCheckedChangeListener(switchImageAsBackgroundListener);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnSettingFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                context.toString()
                    + " has to implement the interface OnSettingFragmentListener"
            );
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setLunchPlanCountText(getContext());
    }

    private void setLunchPlanCountText(Context context) {
        LunchPlanRepo lunchPlanRepo = new LunchPlanRepo(context, Executors.newSingleThreadExecutor());
        disposables.add(lunchPlanRepo.getAllFlowable()
            .subscribeOn(Schedulers.io())
            .map(lunchPlanList -> {
                LunchPlan[] lunchPlanArray = lunchPlanList.toArray(new LunchPlan[lunchPlanList.size()]);
                StringBuilder stringBuilder = new StringBuilder();

                for (LunchPlan lunchPlan : lunchPlanArray) {
                    if (lunchPlan.getBase64Img() != null) {
                        stringBuilder.append(lunchPlan.getBase64Img());
                    }
                }

                return getString(R.string.saved_lunchplans_count)
                    + " "
                    + Integer.toString(lunchPlanArray.length)
                    + " ("
                    + Integer.toString(stringBuilder.toString().getBytes("UTF-8").length / 1024)
                    + " kByte)";
            })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                lunchPlanCountText -> {
                    TextView textView = (TextView) getView().findViewById(R.id.textView_setting_info_2);
                    textView.setText(lunchPlanCountText);
                },
                error -> {}
            )
        );
    }

    CompoundButton.OnCheckedChangeListener switchUseDbListener =
        new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                switch (view.getId()) {
                    case R.id.switch_use_database:
                        if (isChecked) {
                            if (!SettingActivity.getSettingBool(getActivity(), SettingActivity.PREF_SETTING_DB, true)) {
                                mListener.saveSettingBool(SettingActivity.PREF_SETTING_DB, true);
                                Toast.makeText(getContext(), getString(R.string.database_will_be_used_by_now), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(view.getContext());
                            alertBuilder.setTitle(getString(R.string.are_you_sure))
                                .setPositiveButton(getString(R.string.yes), dialogClickListenerDeletingDatabase)
                                .setNegativeButton(getString(R.string.no), dialogClickListenerDeletingDatabase)
                                .show();
                        }

                        break;
                }
            }
        };

    DialogInterface.OnClickListener dialogClickListenerDeletingDatabase =
        new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        mListener.deleteDatabase();
                        mListener.saveSettingBool(SettingActivity.PREF_SETTING_DB, false);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        Switch switchUseDb = getView().findViewById(R.id.switch_use_database);
                        switchUseDb.setChecked(true);
                        break;
                }
            }
        };

    CompoundButton.OnCheckedChangeListener switchImageAsBackgroundListener =
        (view, isChecked) -> {
            switch (view.getId()) {
                case R.id.switch_image_as_background:
                    if (isChecked) {
                        mListener.saveSettingBool(SettingActivity.PREF_SETTING_IMAGE_AS_BACKGROUND, true);
                    } else {
                        mListener.saveSettingBool(SettingActivity.PREF_SETTING_IMAGE_AS_BACKGROUND, false);
                    }

                    break;
            }
        };

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }
}
