package com.tbcanteen;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.tbcanteen.lunch.LunchPlan;
import com.tbcanteen.lunch.LunchPlanDao;

/**
 * entities = tables in db
 */
@Database(entities = {LunchPlan.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract LunchPlanDao getLunchPlanDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                context,
                AppDatabase.class,
                "lunchPlanDb" // Name of the database file.
            ).build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}

