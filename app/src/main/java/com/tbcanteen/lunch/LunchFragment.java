package com.tbcanteen.lunch;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.tbcanteen.R;


/**
 * A simple {@link Fragment} subclass.
 */
abstract public class LunchFragment extends Fragment {

    public static final String CALENDAR_WEEK = "CALENDAR_WEEK";
    protected int calendarWeek;

    abstract public void setActionBarTitle(int week);

    public static LunchFragment getNewInstance(int actualWeek, int calendarWeek) {
        LunchFragment lunchFragment;

        if (calendarWeek == actualWeek) {
            lunchFragment = new ActualLunchFragment();
        } else {
            lunchFragment = new NextLunchFragment();
        }

        lunchFragment.calendarWeek = calendarWeek;

        return lunchFragment;
    }

    public LunchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lunch, container, false);

        return view;
    }

    /**
     * Is called after onCreateView().
     * Should be used as onRestoreInstanceState(), like for activities.
     * <p>
     * getArguments() will "ONLY" used one time for init a fragment.
     * <p>
     * Note: savedInstanceState will be null, if we come back from a addToBackStack(null)!
     * (See comment above onSaveInstanceState())
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Boolean previousState;

        if (savedInstanceState != null) {
            calendarWeek = savedInstanceState.getInt(CALENDAR_WEEK);
            previousState = true;
        } else {
            previousState = false;
        }

        ViewModelProviders.of(getActivity())
            .get(LunchPlanViewModel.class)
            .getLiveLunchPlan(calendarWeek)
            .observe(this, lunchPlan -> updateLunchPlan(previousState, lunchPlan));
    }

    private void updateLunchPlan(Boolean previousState, LunchPlan lunchPlan) {
        ImageView imgViewUserImg = (ImageView) getView().findViewById(R.id.fragment_lunch_image_view_id);

        if (lunchPlan == null) {
            imgViewUserImg.setVisibility(View.GONE);
            hideLoadingShowImage(getView().findViewById(R.id.container_lunch_plan_not_found));

            return;
        }

        // Image-processing.
        byte[] imageBytes = Base64.decode(lunchPlan.getBase64Img(), Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgViewUserImg.setImageBitmap(decodedImage);

        // If img for plan_not_found is visible, hide it.
        View imgViewNotFound = getView().findViewById(R.id.container_lunch_plan_not_found);

        if (imgViewNotFound.getVisibility() == View.VISIBLE) {
            imgViewNotFound.setVisibility(View.GONE);
        }

        // Show plan & hide loading_panel.
        if (!previousState) {
            hideLoadingShowImage(imgViewUserImg);
        } else {
            getView().findViewById(R.id.container_loading_panel).setVisibility(View.GONE);
        }
    }

    private void hideLoadingShowImage(View viewImage) {
        final View loadingPanel = getView().findViewById(R.id.container_loading_panel);

        viewImage.setVisibility(View.INVISIBLE);
        AnimationSet mAnimationSet = new AnimationSet(false); // False means don't share interpolators.
        Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        mAnimationSet.addAnimation(fadeInAnimation);
        viewImage.startAnimation(mAnimationSet);
        viewImage.setVisibility(View.VISIBLE);

        loadingPanel.postDelayed(new Runnable() {
            public void run() {
                loadingPanel.setVisibility(View.GONE);
            }
        }, 500);
    }

    /**
     * Will "not" be called, if you use addToBackStack(null) (click return on another fragment)
     * So we can't rely on savedInstanceState in parent fragment.
     * addToBackStack(null) will hold the instance of the fragment, so we have to save data in this
     * object directly.
     *
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(@NonNull final Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt(CALENDAR_WEEK, calendarWeek);
    }
}
