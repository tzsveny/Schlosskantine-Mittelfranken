package com.tbcanteen.lunch;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.tbcanteen.R;
import com.tbcanteen.setting.SettingActivity;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class LunchActivity extends AppCompatActivity {

    public static final String LUNCH_ADAPTER_COUNT = "LUNCH_ADAPTER_COUNT";

    public static int actualCalendarWeek = 0;
    public static Context appContext;
    private LunchPlanPager lunchPlanPager;
    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appContext = getApplicationContext();

        setContentView(R.layout.activity_lunch);

        Toolbar myToolbar = findViewById(R.id.activity_lunch_toolbar);
        myToolbar.setTitle("");
        myToolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorTradebyteBlue)));
        setSupportActionBar(myToolbar);

        int lunchAdapterCount = 1;

        if (savedInstanceState != null) {
            lunchAdapterCount = savedInstanceState.getInt(LUNCH_ADAPTER_COUNT, 1);
        }

        // Create LunchPlanPager, which will create an pageAdapter, which will contains LunchFragments.
        if (findViewById(R.id.activity_lunch_fragment_container) != null) {
            lunchPlanPager = new LunchPlanPager(
                this,
                getSupportFragmentManager(),
                (ViewPager)findViewById(R.id.activity_lunch_fragment_container),
                savedInstanceState == null,
                lunchAdapterCount
            );
        }

        if (savedInstanceState == null) {
            // On each app start, look for new plans.
            syncLunchPlanPagerNumber();

            // On each app start, check if there are old LunchPlans to delete
            cleanUpDatabase();
        }

        // Look for new plans periodically.
        // (Only, if after a small delay the ViewModel still has no LunchPlans for actual week)
        checkForLunchPlansPeriodically();
    }

    private void checkForLunchPlansPeriodically() {
        final CompositeDisposable timerDisposer = new CompositeDisposable();

        timerDisposer.add(Observable.interval(7000, 3000, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(aLong -> {
                MutableLiveData<LunchPlan> liveLunchPlan = ViewModelProviders.of(this).get(LunchPlanViewModel.class)
                    .getLiveLunchPlanHashMap().get(getActualCalendarWeek());

                if (liveLunchPlan.getValue() == null) {
                    syncLunchPlanPagerNumber();
                } else {
                    disposables.remove(timerDisposer);
                    timerDisposer.dispose();
                }
            }));

        disposables.add(timerDisposer);
    }

    private void syncLunchPlanPagerNumber() {
        disposables.add(ViewModelProviders.of(this).get(LunchPlanViewModel.class)
            .getLunchPlanObservable(true)
            .filter(lunchPlan -> lunchPlan.getCalendarWeek() >= LunchActivity.getActualCalendarWeek())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                lunchPlan -> lunchPlanPager.updateLunchPlanNumber(lunchPlan.getCalendarWeek()),
                error -> {},
                this::checkDbIntegrity
            )
        );
    }

    /**
     * Check integrity of database and delete plans, if they have changed remotely.
     */
    private void checkDbIntegrity() {
        if (!SettingActivity.getSettingBool(getApplication(), SettingActivity.PREF_SETTING_DB, true)) {
            return;
        }

        LunchPlanRepo lunchPlanRepo = new LunchPlanRepo(getApplication(), Executors.newSingleThreadExecutor());

        disposables.add(ViewModelProviders.of(this).get(LunchPlanViewModel.class)
            .getLunchPlanObservable(false)
            .flatMap(lunchPlan ->
                lunchPlanRepo.getLunchPlan(lunchPlan.getCalendarWeek())
                    .subscribeOn(Schedulers.io())
                    .onErrorReturn(error -> new LunchPlan())
                    .map(dbLunchPlan -> new Pair<>(lunchPlan, dbLunchPlan))
                    .toObservable()
            )
            .filter(pair -> pair.first.getLunchPlanId() != null)
            .filter(pair -> pair.second.getLunchPlanId() != null)
            .filter(pair -> !pair.first.getLunchPlanId().equals(pair.second.getLunchPlanId()))
            .subscribe(
                pair -> lunchPlanRepo.delete(((LunchPlan) pair.second)),
                error -> {},
                () -> {} // onComplete consumer is needed.
            )
        );
    }

    private void cleanUpDatabase() {
        if (SettingActivity.getSettingBool(appContext, SettingActivity.PREF_SETTING_DB, true)) {
            LunchPlanRepo lunchPlanRepo = new LunchPlanRepo(appContext, Executors.newSingleThreadExecutor());
            lunchPlanRepo.deleteOldPlans(getActualCalendarWeek());

            // In the first 3 weeks of new year, delete also all plans from last year.
            if (getActualCalendarWeek() < 4) {
                lunchPlanRepo.deleteLastYearPlans(getActualCalendarWeek());
            }
        }
    }

    public static Integer getActualCalendarWeek() {
        if (actualCalendarWeek == 0) {
            actualCalendarWeek = Integer.parseInt(new SimpleDateFormat("w", Locale.GERMANY).format(new java.util.Date()));
        }

        return actualCalendarWeek;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_lunch, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_setting_activity:
                startSettingActivity();
                return true;

            case android.R.id.home:
                if (lunchPlanPager.getCurrentPageItem() == 0) {
                    return super.onOptionsItemSelected(item);
                } else {
                    lunchPlanPager.setCurrentPageItem(lunchPlanPager.getCurrentPageItem() - 1);
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startSettingActivity() {
        Intent intent = new Intent(LunchActivity.appContext, SettingActivity.class);

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (lunchPlanPager.getCurrentPageItem() == 0) {
            super.onBackPressed();
        } else {
            lunchPlanPager.setCurrentPageItem(lunchPlanPager.getCurrentPageItem() - 1);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt(LUNCH_ADAPTER_COUNT, lunchPlanPager.getNumberOfPages());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }
}
