package com.tbcanteen.lunch;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "lunch_plans")
public class LunchPlan implements java.io.Serializable, Parcelable {

    @PrimaryKey
    @ColumnInfo(name = "calendar_week")
    private int calendarWeek;

    @ColumnInfo(name = "lunch_plan_id")
    private String lunchPlanId;

    @ColumnInfo(name = "url")
    private String url;

    @ColumnInfo(name = "image")
    private String base64Img;

    // This field will not persisted in the db.
    @Ignore
    private String example;

    // For parcelable: This is used to regenerate your object.
    // All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<LunchPlan> CREATOR = new Parcelable.Creator<LunchPlan>() {
        public LunchPlan createFromParcel(Parcel in) {
            return new LunchPlan(in);
        }

        public LunchPlan[] newArray(int size) {
            return new LunchPlan[size];
        }
    };

    public LunchPlan() {
    }

    // For parcelable: Takes a Parcel and gives an object populated with it's values
    // (Has to be the same order, like in writeToParcel)
    LunchPlan(Parcel in) {
        setCalendarWeek(in.readInt());
        setLunchPlanId(in.readString());
        setUrl(in.readString());
        setBase64Img(in.readString());
    }

    // For parcelable: Write object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(getCalendarWeek());
        out.writeString(getLunchPlanId());
        out.writeString(getUrl());
        out.writeString(getBase64Img());
    }

    // For parcelable: (can be ignored in 99,9% of cases)
    @Override
    public int describeContents() {
        return 0;
    }

    public void setCalendarWeek(int calendarWeek) {
        this.calendarWeek = calendarWeek;
    }

    public int getCalendarWeek() {
        return calendarWeek;
    }

    public void setLunchPlanId(String lunchPlanId) {
        this.lunchPlanId = lunchPlanId;
    }

    public String getLunchPlanId() {
        return lunchPlanId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setBase64Img(String image) {
        this.base64Img = image;
    }

    public String getBase64Img() {
        return base64Img;
    }
}
