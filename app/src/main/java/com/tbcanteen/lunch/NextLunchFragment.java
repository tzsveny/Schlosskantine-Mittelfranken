package com.tbcanteen.lunch;

import android.support.v7.app.AppCompatActivity;
import android.text.Html;

import com.tbcanteen.R;

public class NextLunchFragment extends LunchFragment {
    public void setActionBarTitle(int week) {
        String headerText = getResources().getString(R.string.toolbar_lunch_activity_next);

        if ((week - 1) > LunchActivity.getActualCalendarWeek()) {
            headerText = Integer.toString(week-LunchActivity.getActualCalendarWeek()+1)
                + getResources().getString(R.string.toolbar_lunch_activity_next_next);
        }

        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(
                Html.fromHtml(
                    "<font color='" + getResources().getColor(R.color.colorYellow) + "'>"
                        + getResources().getString(R.string.toolbar_lunch_cw)
                        + " " + Integer.toString(week) + "</font>"
                        + "&nbsp;&nbsp;&nbsp;&nbsp;"
                        + "<font color='" + getResources().getColor(R.color.colorWhite) + "'>"
                        + headerText
                        + "</font>"
                )
            );

            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
