package com.tbcanteen.lunch;

import android.content.Context;

import com.tbcanteen.AppDatabase;

import java.util.List;
import java.util.concurrent.ExecutorService;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LunchPlanRepo implements LunchPlanDao {
    private LunchPlanDao lunchPlanDao;
    private ExecutorService executorService;

    public LunchPlanRepo(Context context, ExecutorService executor) {
        this.lunchPlanDao = AppDatabase.getAppDatabase(context).getLunchPlanDao();
        this.executorService = executor;
    }

    public Single<List<LunchPlan>> getAll() {
        return lunchPlanDao.getAll();
    }

    public Flowable<List<LunchPlan>> getAllFlowable() {
        return lunchPlanDao.getAllFlowable();
    }

    public Single<LunchPlan> getLunchPlan(int week) {
        return lunchPlanDao.getLunchPlan(week);
    }

    public void insert(final LunchPlan... lunchPlan) {
        executorService.execute(
            new Runnable() {
                @Override
                public void run() {
                    lunchPlanDao.insert(lunchPlan);
                }
            }
        );
    }

    public void delete(final LunchPlan... lunchPlan) {
        executorService.execute(
            new Runnable() {
                @Override
                public void run() {
                    lunchPlanDao.delete(lunchPlan);
                }
            }
        );
    }

    public void deleteOldPlans(final int actualWeek) {
        executorService.execute(
            new Runnable() {
                @Override
                public void run() {
                    lunchPlanDao.deleteOldPlans(actualWeek);
                }
            }
        );
    }

    public void deleteLastYearPlans(final int actualWeek) {
        executorService.execute(
            new Runnable() {
                @Override
                public void run() {
                    lunchPlanDao.deleteLastYearPlans(actualWeek);
                }
            }
        );
    }

    public void deleteAll() {
        // Executor should be preferred, instead of new Thread()
        executorService.execute(
            new Runnable() {
                @Override
                public void run() {
                    lunchPlanDao.deleteAll();
                }
            }
        );
    }
}
