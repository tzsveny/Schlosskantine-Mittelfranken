package com.tbcanteen.lunch;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * DAO: Contains the methods used for accessing the database.
 */
@Dao
public interface LunchPlanDao {
    @Query("SELECT * FROM lunch_plans")
    Single<List<LunchPlan>> getAll();

    @Query("SELECT * FROM lunch_plans")
    Flowable<List<LunchPlan>> getAllFlowable();

    @Query("SELECT * FROM lunch_plans WHERE calendar_week = :week")
    Single<LunchPlan> getLunchPlan(int week);

    @Insert
    void insert(LunchPlan... lunchPlan);

    @Delete
    void delete(LunchPlan... lunchPlan);

    @Query("DELETE FROM lunch_plans WHERE calendar_week < :actualWeek")
    void deleteOldPlans(int actualWeek);

    @Query("DELETE FROM lunch_plans WHERE (calendar_week + 5) > :actualWeek")
    void deleteLastYearPlans(int actualWeek);

    @Query("DELETE FROM lunch_plans")
    void deleteAll();
}
