package com.tbcanteen.lunch;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.SparseArray;

import com.tbcanteen.UrlToBase64;
import com.tbcanteen.TwitterTimeLine;
import com.tbcanteen.setting.SettingActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A object of an "Android"ViewModel will always hold a reference to the called activity.
 * A ViewModel is independent of (only!) "configuration changes" (e.g. rotation changes)
 * and gets cleared when activity gets destroyed (ViewModel.onCleared()).
 * (Note: In both cases Activity's onDestroy() will be called)
 * See https://developer.android.com/images/topic/libraries/architecture/viewmodel-lifecycle.png
 *
 * More detailed flow:
 *  - ViewModelProviders will add a HolderFragment for the activity, if it hasn't been already.
 *  - HolderFragment is setRetainInstance(true) => means the fragment will retains its instance between "configuration changes".
 *  - HolderFragment has a ViewModelStore, which holds ViewModels.
 *  See https://medium.com/@Viraj.Tank/android-architecture-components-viewmodel-internals-should-you-use-it-for-mvps-presenter-6bf7b770cf12
 */
public class LunchPlanViewModel extends AndroidViewModel {

    /**
     * SparseArray (more memory efficient) = HashMap<Integer, MutableLiveData<LunchPlan>>
     */
    private SparseArray<MutableLiveData<LunchPlan>> liveLunchPlanHashMap;
    private Observable<LunchPlan> lunchPlanObservable;
    private CompositeDisposable disposables;

    LunchPlanViewModel(Application application) {
        super(application);

        disposables = new CompositeDisposable();

        if (liveLunchPlanHashMap == null) {
            liveLunchPlanHashMap = new SparseArray<>();
        }
    }

    public SparseArray<MutableLiveData<LunchPlan>> getLiveLunchPlanHashMap() {
        return liveLunchPlanHashMap;
    }

    public void setLiveLunchPlanHashMap(SparseArray<MutableLiveData<LunchPlan>> liveLunchPlanHashMap) {
        this.liveLunchPlanHashMap = liveLunchPlanHashMap;
    }

    /**
     * MutableLiveData is mutable, whereas LiveData is immutable.
     *
     * @param calendarWeek
     * @return
     */
    public LiveData<LunchPlan> getLiveLunchPlan(int calendarWeek) {
        if (liveLunchPlanHashMap.get(calendarWeek) == null) {
            MutableLiveData<LunchPlan> liveLunchPlan = new MutableLiveData<>();
            liveLunchPlanHashMap.put(calendarWeek, liveLunchPlan);
        }

        if (SettingActivity.getSettingBool(getApplication(), SettingActivity.PREF_SETTING_DB, true)) {
            getLunchPlanFromDb(calendarWeek);
        } else {
            fetchLunchPlanFromTwitter(calendarWeek);
        }

        return liveLunchPlanHashMap.get(calendarWeek);
    }

    private void getLunchPlanFromDb(final int calendarWeek) {
        LunchPlanRepo lunchPlanRepo = new LunchPlanRepo(getApplication(), Executors.newSingleThreadExecutor());

        /* When there was something found, Maybe will trigger onSuccess and it will complete.
         * When there was nothing found, Maybe will complete (Single will trigger onError(EmptyResultSetException.class))
         */
        disposables.add(lunchPlanRepo.getLunchPlan(calendarWeek)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                lunchPlan -> updateLiveLunchPlan(calendarWeek, lunchPlan),
                throwable -> fetchLunchPlanFromTwitter(calendarWeek)
            )
        );
    }

    /**
     * If DB is activated, but there are maybe only 2 of 3 plans saved, we only need to fetch the last one.
     */
    private void fetchLunchPlanFromTwitter(int calendarWeek) {
        disposables.add(getLunchPlanObservable(false)
            .filter(lunchPlan -> lunchPlan.getCalendarWeek() == calendarWeek)
            .take(1)
            .singleOrError()
            .flatMap(lunchPlan -> UrlToBase64.getBase64FromUrl(lunchPlan.getUrl())
                .subscribeOn(Schedulers.io())
                .onErrorReturn(error -> "")
                .map(base64Img -> {
                    lunchPlan.setBase64Img(base64Img);
                    return lunchPlan;
                })
            )
            .filter(lunchPlan -> !"".equals(lunchPlan.getBase64Img()))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                lunchPlan -> {
                    updateLiveLunchPlan(lunchPlan.getCalendarWeek(), lunchPlan);

                    if (SettingActivity.getSettingBool(getApplication(), SettingActivity.PREF_SETTING_DB, true)) {
                        LunchPlanRepo lunchPlanRepo = new LunchPlanRepo(getApplication(), Executors.newSingleThreadExecutor());
                        lunchPlan.setLunchPlanId(lunchPlan.getLunchPlanId());
                        lunchPlanRepo.insert(lunchPlan);
                    }
                },
                error -> updateLiveLunchPlan(calendarWeek, null),
                () -> {} // onComplete consumer is needed.
            )
        );
    }

    /**
     * @param calendarWeek Week, to find the correct fragment.
     * @param lunchPlan LunchPlan|null
     */
    private void updateLiveLunchPlan(int calendarWeek, LunchPlan lunchPlan) {
        if (liveLunchPlanHashMap.get(calendarWeek) == null) {
            MutableLiveData<LunchPlan> liveLunchPlan = new MutableLiveData<>();
            liveLunchPlanHashMap.put(calendarWeek, liveLunchPlan);
        }

        liveLunchPlanHashMap.get(calendarWeek).setValue(lunchPlan);
    }

    public Observable<LunchPlan> getLunchPlanObservable(Boolean fetchInitially) {
        if (lunchPlanObservable == null || fetchInitially) {
            lunchPlanObservable = TwitterTimeLine.getTwitterTimeLineObservable("tbkantine")
                .subscribeOn(Schedulers.io())
                .map(LunchPlanViewModel::buildLunchPlan)
                .cache();
        }

        return lunchPlanObservable;
    }

    private static LunchPlan buildLunchPlan(JSONObject tweetAll) {
        LunchPlan lunchPlan = new LunchPlan();

        try {
            JSONObject tweet = tweetAll.getJSONObject("tweet");
            String tweetText = tweet.getString("text");
            int calendarWeek = Integer.parseInt(tweetText.substring(2, 4).trim());


            lunchPlan.setCalendarWeek(calendarWeek);

            JSONArray jsonArrayMedia = tweetAll.getJSONArray("media");
            JSONObject media = jsonArrayMedia.getJSONObject(0);
            lunchPlan.setLunchPlanId(media.getString("id"));
            lunchPlan.setUrl(media.getString("url"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return lunchPlan;
    }

    @Override
    public void onCleared() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }
}
