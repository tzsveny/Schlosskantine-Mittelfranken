package com.tbcanteen.lunch;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.ViewGroup;


import com.tbcanteen.R;
import com.tbcanteen.setting.SettingActivity;

import static com.tbcanteen.lunch.LunchActivity.getActualCalendarWeek;

public class LunchPlanPager {
    private LunchPageAdapter mAdapter;
    private ViewPager mPager;
    private SparseArray<LunchFragment> lunchFragmentHashMap;

    LunchPlanPager (Context context, FragmentManager fragmentManager, ViewPager viewPager, Boolean initialLoad, int lunchAdapterCount) {
        mAdapter = new LunchPageAdapter(fragmentManager, lunchAdapterCount);
        mPager = viewPager;

        if (SettingActivity.getSettingBool(context, SettingActivity.PREF_SETTING_IMAGE_AS_BACKGROUND, true)) {
            mPager.setBackgroundResource(R.drawable.canteen);
        }

        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(1); // 1 = Default

        mPager.addOnPageChangeListener(new LunchPlanPageChangeListener());

        if (initialLoad) {
            // If we come from a previous state, the current item is last one. Otherwise it will set to 0.
            mPager.setCurrentItem(0);
        }
    }

    /**
     * Set title of activity dynamically.
     */
    private class LunchPlanPageChangeListener implements ViewPager.OnPageChangeListener {
        Boolean initializeTitle = true;

        public void onPageScrollStateChanged(int state) {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (lunchFragmentHashMap != null
                && lunchFragmentHashMap.get(getActualCalendarWeek() + position) != null
                && initializeTitle) {
                lunchFragmentHashMap.get(getActualCalendarWeek() + position).setActionBarTitle(getActualCalendarWeek() + position);
                initializeTitle = false;
            }
        }

        public void onPageSelected(int position) {
            if (lunchFragmentHashMap != null
                && lunchFragmentHashMap.get(getActualCalendarWeek() + position) != null) {
                lunchFragmentHashMap.get(getActualCalendarWeek() + position).setActionBarTitle(getActualCalendarWeek() + position);
            }
        }
    }

    /**
     * A simple pager adapter that represents 2 Fragment objects, in sequence.
     */
    private class LunchPageAdapter extends FragmentStatePagerAdapter {
        private int lunchAdapterCount;

        LunchPageAdapter(FragmentManager fm, int lunchPlanCount) {
            super(fm);
            lunchFragmentHashMap = new SparseArray<>();

            setCount(lunchPlanCount);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // super.instantiateItem() will call getItem().
            LunchFragment createdFragment = (LunchFragment) super.instantiateItem(container, position);

            lunchFragmentHashMap.put(getActualCalendarWeek() + position, createdFragment);

            return createdFragment;
        }

        @Override
        public Fragment getItem(int position) {
            return LunchFragment.getNewInstance(getActualCalendarWeek(), getActualCalendarWeek() + position);
        }

        @Override
        public int getCount() {
            return lunchAdapterCount;
        }

        private void setCount(int lunchPlanCount) {
            if (lunchPlanCount == 0) {
                this.lunchAdapterCount = 1;
            } else {
                this.lunchAdapterCount = lunchPlanCount;
            }
        }

        public void updateLunchAdapterCount(int calendarWeek) {
            int lunchPlanCount = calendarWeek - getActualCalendarWeek() + 1;

            if (lunchPlanCount > getCount()) {
                setCount(lunchPlanCount);
                notifyDataSetChanged();
            }
        }
    }

    public void updateLunchPlanNumber(int calendarWeek) {
        mAdapter.updateLunchAdapterCount(calendarWeek);
    }

    public int getCurrentPageItem() {
        return mPager.getCurrentItem();
    }

    public void setCurrentPageItem(int pageNumber) {
        mPager.setCurrentItem(pageNumber);
    }

    public int getNumberOfPages() {
        return mAdapter.getCount();
    }
}
