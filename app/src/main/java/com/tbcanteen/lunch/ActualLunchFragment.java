package com.tbcanteen.lunch;

import android.support.v7.app.AppCompatActivity;
import android.text.Html;

import com.tbcanteen.R;

import java.text.SimpleDateFormat;

public class ActualLunchFragment extends LunchFragment {
    public void setActionBarTitle(int week) {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(
                Html.fromHtml(
                    "<font color='" + getResources().getColor(R.color.colorYellow) + "'>"
                        + getResources().getString(R.string.toolbar_lunch_cw)
                        + " " + Integer.toString(week)
                        + "</font>"
                        + "&nbsp;&nbsp;&nbsp;&nbsp;"
                        + "<font color='" + getResources().getColor(R.color.colorWhite) + "'>"
                        + new SimpleDateFormat("EEEE").format(new java.util.Date())
                        + ", "
                        + new SimpleDateFormat("dd.MM.yy").format(new java.util.Date())
                        + "</font>"
                )
            );

            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
