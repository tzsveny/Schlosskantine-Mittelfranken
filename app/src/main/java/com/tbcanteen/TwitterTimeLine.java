package com.tbcanteen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.Observable;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterTimeLine implements TwitterSetting {

    public static Observable<JSONObject> getTwitterTimeLineObservable(final String twitterUser) {
        return Observable.create(subscriber -> {
            ConfigurationBuilder cb = new ConfigurationBuilder();
            cb.setDebugEnabled(true)
                .setOAuthConsumerKey(CONSUMER_KEY)
                .setOAuthConsumerSecret(CONSUMER_SECRET)
                .setOAuthAccessToken(ACCESS_TOKEN)
                .setOAuthAccessTokenSecret(ACCESS_SECRET);

            TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();

            try {
                List<Status> statuses;
                statuses = twitter.getUserTimeline(twitterUser);

                for (twitter4j.Status status : statuses) {
                    JSONObject jsonObjectEntity = new JSONObject();

                    try {
                        // Add Tweet
                        JSONObject jsonObjectTweet = new JSONObject();
                        jsonObjectTweet.put("id", status.getId());
                        jsonObjectTweet.put("text", status.getText());
                        jsonObjectTweet.put("created", status.getCreatedAt());
                        jsonObjectEntity.put("tweet", jsonObjectTweet);

                        // Add User
                        JSONObject jsonObjectUser = new JSONObject();
                        jsonObjectUser.put("id", status.getUser().getId());
                        jsonObjectUser.put("name", status.getUser().getScreenName());
                        jsonObjectEntity.put("user", jsonObjectUser);

                        // Add Media
                        MediaEntity[] media = status.getMediaEntities(); //get the media entities from the status
                        JSONArray jsonArrayMedia = new JSONArray();

                        for (MediaEntity m : media) {
                            JSONObject jsonObjectMedia = new JSONObject();
                            jsonObjectMedia.put("id", m.getId());
                            jsonObjectMedia.put("url", m.getMediaURL());
                            jsonArrayMedia.put(jsonObjectMedia);
                        }

                        jsonObjectEntity.put("media", jsonArrayMedia);

                        subscriber.onNext(jsonObjectEntity);
                    } catch (JSONException e) {
                        subscriber.onError(e);
                    }
                }
            } catch (TwitterException e) {
                subscriber.onError(e);
            }

            subscriber.onComplete();
        });
    }
}
