package com.tbcanteen;

import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import io.reactivex.Single;

public class UrlToBase64 {
    public static Single<String> getBase64FromUrl(String url) {
        return Single.create(subscriber -> {
            InputStream inputStream = null;

            try {
                inputStream = new URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (inputStream != null) {
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();

                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    subscriber.onError(e);
                }

                bytes = output.toByteArray();

                subscriber.onSuccess(Base64.encodeToString(bytes, Base64.DEFAULT));
            }
        });
    }
}
